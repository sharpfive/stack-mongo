#!/usr/bin/env ruby

require 'rubygems'
require 'mongo'
require 'crack'

include Mongo

def putFileInMongo(fileName, db)
  puts "open file:" + fileName
  file = open(fileName)
  data = file.read
  
  #
  xmlData = Crack::XML.parse(data)
  xmlData.each{
    |key, value| #top level xml element
    elementString = key.to_s
    
    #puts "creating collection:" + elementString
    #create a collection using the key for the top level element of the file
    
    collection = db.collection(elementString)
    collection.drop();
    
    begin
      # bulk insert FTW!
      collection.insert(xmlData[key]['row'])
    rescue
      #puts "  falling back to inserting each row"
      xmlData[key]['row'].each { |row| collection.insert(row) }
    end
  }

end


#start your engines

directory = ARGV[0]
databaseName = ARGV[1]

startTime = Time.now

puts "open mongodb"
mongo_client = MongoClient.new("localhost", 27017)
db = mongo_client.db(databaseName)

Dir.chdir(directory)
puts "Get files from directory:" + directory
xmlFiles = Dir.glob("*.{xml}")
puts xmlFiles.count.to_s + " files found"

xmlFiles.each{ |xmlFile| putFileInMongo(xmlFile, db) }

stopTime = Time.now

puts "done. " + "Time elapsed: " + (stopTime.to_i - startTime.to_i).to_s + " seconds"
